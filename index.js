console.log("Hello Earth")


				// Array Methods
/*
    -Javascript has built-in function and methods for arrays. This allows us to manipulate and access array items.
    -Array can be either mutated or iterated
        - Array "mutations" seek to modify the contents of an array while array "iteration" aim to evaluate and loop over each element.
*/


// Mutator Methods - functions/methods that mutate or chang an array
//                 - manipultaes the original array performimg task such as adding and removing elements


console.log("==Mutator Methods");
console.log("-------------");
let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit"];



console.log("=>Push"); //can add multiple arguments
console.log("fruits array: ");
console.log(fruits);

let fruitslength = fruits.push("Mango");
console.log(fruits);


fruits.push("Avocado", "Guava"); //can add multiple arguments
console.log(fruits);
//console.log(fruits.push("Avocado", "Guava"));

/*function addMultipleFruits(fruit1, fruit2, fruit3){
	fruits.push(fruit1, fruit2, fruit3);
	console.log(fruits);
}

addMultipleFruits("Durian", "Atis", "Melon")*/


// Pop Method - removes the last element in an array and returns the removed element

console.log("-------------");
let removedFruit=fruits.pop();
console.log(removedFruit); // returns the removed element
console.log(fruits)

//UNSHIFT

/*
    - Add one or more elements at the beginning of an array.
    - Syntax:
        arrayName.unshift("elementA");
        arrayName.unshift("elementA", "elementB");
*/


console.log("-------------");

fruits.unshift("Lime", "Banana");
console.log(fruits);


console.log("-------------");

//SHIFT - remove an element at the beginning of an array and returns the returns the removed statements

let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log(fruits);

console.log("-------------");

//SPLICE

/*
    - simulatanously removes an element from a specified index number and adds new elements.
    - Syntax:
        arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)
    - can also be used to delete/remove elements(w/o declaring replacement element)
*/

// current array ['Banana', 'Apple', 'Orange', 'Kiwi', 'Dragon Fruit', 'Mango', 'Avocado']
fruits.splice(1,2,"Lime","Cherry", "Tomato");
console.log(fruits);

console.log("-------------");


//SORT
	/*
	    -Rearranges the array elements in alphanumeric order
	    -Syntax:
	        - arrayName.sort();
	*/


fruits.sort();
console.log(fruits);

console.log("-------------");

//Reverse
/*
    - Reverse the order of array elements
    - Syntax:
        arrayName.reverse();
*/

fruits.reverse();
console.log(fruits);

console.log("-------------");




//  Non-mutator
	// functions that do not modify or change an array after they are created


console.log("-----------");
console.log("-----------");
console.log("==Mutator Methods==");
console.log("-----------");

let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];
console.log(countries);
// indexOf()
/*
    - Returns the index of the first matching element found in an array.
    - If no match was found, the result will be -1.
    - The search process will bne done from the first element proceeding to the last element.
    - Syntax:
        arrayName.indexOf(searchValue);
        arrayName.indexOf(searchValue, fromIndex/startingIndex);
*/

let firstIndex = countries.indexOf("PH");
console.log(firstIndex);

/*let firstIndex = countries.indexOf("PH", 2); //with a specified index of start
console.log(firstIndex);*/

let invalidCountry = countries.indexOf("BR");
console.log(invalidCountry);

console.log("-----------");


// lastIndexOf()
/*
    - Returns the index number of the last matching element found in an array.
    - The search from process will be done from the last element proceeding to the first element.
    - Syntax:
        arrayName.lastIndexOf(searchValue);
        arrayName.lastIndexOF(searchValue, fromIndex/EndingIndex);
*/


let lastIndex = countries.lastIndexOf("PH");
console.log(lastIndex);
												//index #
let lastIndexStart = countries.lastIndexOf("PH", 4);
console.log(lastIndexStart);

console.log("-----------");

// slice()
/*
    - Portions/slices element from an array AND returns a new array.
    - Syntax:
        arrayName.slice(startingIndex); //until the last element of the array.
        arrayName.slice(startingIndex, endingIndex);
*/


console.log(countries);

// Slicing of elements from specified index to the last element
//   0		1     2      3     4
// ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE']	
let sliceArrayA = countries.slice(2);
    console.log(sliceArrayA);

// Slicing off element from specified index to another index. But the specified last index is not included in the return.
    let sliceArrayB = countries.slice(2, 5); //2 -> 4 // ends with the last declared index minus one
    console.log("Result from slice(2, 5):");
    console.log(sliceArrayB);


let sliceArrayC = countries.slice(-3);
    console.log("Result from slice method:");
    console.log(sliceArrayC);

console.log("-----------");

// toString
/*
    - Returns an array as a string, separated by commas.
    - Syntax 
        arrayName.toString();
*/


// ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];
let stringArray = countries.toString();
console.log(stringArray);
console.log(stringArray[0]);
console.log(typeof stringArray); //to check if the array is converted to string.

console.log("-----------");

// Concat - shortcut of concatination

/*
    - combines two arrays AND returns the combined result.
    - Syntax:
        arrayA.concat(arrayB);
        arrayA.concat(element);
*/


let tasksArrayA = ["drink html","eat javascript"];
let tasksArrayB = ["inhale css", "breathe mongoDB"];
let tasksArrayC = ["get git", "be node"];

console.log("tasksArrayA: ");
console.log(tasksArrayA);
console.log("tasksArrayB: ");
console.log(tasksArrayB);
console.log("tasksArrayC: ");
console.log(tasksArrayC);


let tasks = tasksArrayA.concat(tasksArrayB);
console.log(tasks);


// Combining multiple arrays

let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);
console.log(allTasks);


// Combining array with elements
let combineTasks = tasksArrayA.concat("smell express", "throw react");
console.log(combineTasks)

console.log("-----------");

// join()

/*
    - Returns an array as a string separated by specified separator string.
    - Syntax"
        arrayName.join("separatorString");
*/

let users = ["Josephine", "Enola", "John Snow", "Gambit"];
console.log(users);

console.log(users.join());
console.log(users.join(""));
console.log(users.join(" "));
console.log(users.join(" - "));

console.log("-----------");

// Iteration  Methods

/*
    - Similar to a for loop that iterates on each array element.
    -  For each item in the array, the anonymous function passed in the forEach() method will be run.
    - The anonymous function is able to received the current item being iterated or loop over.
    - Variable names for arrays are written in plural form of the data stored.
    - It's common practice to use the singular form of the array content for parameter names used in array loops.
    - forEach() does not return anything.
- Syntax:
        arrayName.forEach(function(indivElement){
            //statement/code block.
        })
*/
   
console.log("----------->for loop");

/*

let tasksArrayA = ["drink html","eat javascript"];
let tasksArrayB = ["inhale css", "breathe mongoDB"];
let tasksArrayC = ["get git", "be node"];
*/
 //initialization  / condition      / chnge of value
for(let index=0; index<allTasks.length; index++){
    console.log(allTasks[index]);
}



console.log("----------->for each");
// after executing code inside the loop, the program will evaluate the condition again.


let filteredTasks = [];  // store in this variablea all task with characters greater than 10.

    // shortcut loop for arrays
    //"task" - // anonymous function "parameter" represents each element of the array to be iterated.

allTasks.forEach(function(task){
    console.log(task); 

    // if the element/string's length is greater than 10 characters
    if(task.length>10){
        filteredTasks.push(task);
    }
});

console.log(filteredTasks);


console.log("-----------");

// map()
/*
    - Iterates on each element and returns new array with different values depending on the result of the function's operation.
    - This is useful for performing tasks where mutating/changing the elements required.

    -Syntax
        let/const resultArray = arrayName.map(function(indivElement));
*/

console.log("=>map()");
let numbers = [1, 2, 3, 4, 5];

// return squared values of each element
let numbersMap = numbers.map(function(number){
    return number * number;
});

console.log(numbers);
console.log(numbersMap);


//map() vs forEach()
    console.log("-----------");  // for each cannot store
    console.log("map() vs forEach()");
    let numberForEach = numbers.forEach(function(number){
        // console.log(number*number);
        return number*number;
    });

    console.log(numberForEach);


console.log("-----------");

// every()
/*
    - checks if ALL elements in an array meet the given condition. 
    - This is useful for validation data stored in arrays especially when dealing with large amounts of data.
    - Returns a true value if all elements meet the condition and false if otherwise.
    -Syntax:
        let/const = resutlArray = arrayName.every(function(indivual element){
            return expression/condition;
        })
*/

numbers = [5, 7, 9, 1, 5];
    // - checks if ALL elements in an array meet the given condition. 
    let allValid = numbers.every(function(number){
        return (number < 3);
    });
    
    console.log("Result of every method: ");
    console.log(allValid);

console.log("-----------");

// some()
    /*
        - Check if at least one element in the array meets the given condition.
        - Returns a true if  at least one elements meets the conditon and false otherwise.
        -Syntax:
            let/const resultArray = arrayName.some(function(individElement){
                return expression/condition;
            })
    */


console.log("=>some()");
// - Check if at least one element in the array meets the given condition.
let someValid = numbers.some(function(number){
    return (number < 2);
})

console.log(someValid);

console.log("-----------");

// filter()
/*
    - Return a new array that contaions elements which meets the given condition.
    - Return an empty array if no elements were found.
    - Useful for filtering array elements with given condition and shortens the syntax.
    - Syntax:
        let/const resultArray = arrayName.filter(function(indivElement){
            return expression/condition:
        })
*/

console.log("=>filter()");
numbers = [1, 2, 3, 4, 5];
    // filterValid - variable where we store the result
    // numbers - arrayname where we base
    //filter - method keyword
    //function and parameter

let filterValid = numbers.filter(function(number){
    return (number < 3);
});

console.log("Result of filter method:");
console.log(filterValid);


// No elements found
let nothingFound = numbers.filter(function(number){
    return number == 0;
})
console.log("Result of filter method: ");
console.log(nothingFound);


// shorten the syntax

/*
let tasksArrayA = ["drink html","eat javascript"];
let tasksArrayB = ["inhale css", "breathe mongoDB"];
let tasksArrayC = ["get git", "be node"];
*/
filteredTasks = allTasks.filter(function(task){
    return (task.length > 10);
})


console.log("-----------");

// includes()
/*
    - checks if the argument passed can be found in the array.
    - it returns a boolean which can be saved in a variable
    - Syntax:
        let/const variableName = arrayName.include(<argumentToFind>);
*/

// efficient for searching inside an array

let products = ["Mouse", "Keyboard", "Laptop", "Monitor"];

let productFound = products.includes("Mouse");
console.log("Result of includes method: ");
console.log(productFound); //returns true


let productNotFound = products.includes("Headset");
console.log("Result of includes method:");
console.log(productNotFound);

    
    console.log("-----------");

// Method chaining

    // The result of the inner method is used in the outer method until all "chained" methods have been resolved. 


    console.log("[Method chaining]");
    //products = ["Mouse", "Keyboard", "Laptop", "Monitor"];

    //filter products array, display only products with letter a
    let filteredProducts = products.filter(function(product){ //returns the element
                //keyboard
        return product.toLowerCase().includes('a'); // returns T or F
    })
    console.log("Result of the chained method:");
    console.log(filteredProducts);

    console.log("-----------");

    // reduce()
/*
    - Evaluates elements from left and right and returns/reduces the array into a single value.
    - Syntax:
        let/const resultVariable = arrayName.reduce(function(accumulator, currentValue){
            return expression/operation
        });
            - accumulator parameter stores the result for every loop.
            - currentValue parameter refers to the current/next element in the array that is evaluated in each iteration of the loop. 
*/

console.log("=>reduce");
let i = 0;

numbers = [1,2,3,4,5];

let reducedArray = numbers.reduce(function(acc, cur){
    // console.warn("current iteration " + ++i);
    console.log("accumulator: " +acc);   //1 /3 /6 /10
    console.log("current value:" + cur); //2 /3 /4 /5 

    // The operation or expression to reduce the array into a single value.
    return acc + cur; //3 /6 /10 /15
});

console.log("Result of reduce method: " +reducedArray);


